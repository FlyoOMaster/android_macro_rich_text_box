package healthcare.powerfullspanstesting;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import healthcare.powerfullspanstesting.dialogs.collection.ListDialogItem;
import healthcare.powerfullspanstesting.macros.CheckboxListMacro;
import healthcare.powerfullspanstesting.macros.DateMacro;
import healthcare.powerfullspanstesting.macros.DateTimeMacro;
import healthcare.powerfullspanstesting.macros.NumberMacro;
import healthcare.powerfullspanstesting.macros.TextMacro;
import healthcare.powerfullspanstesting.macros.TimeMacro;
import healthcare.powerfullspanstesting.macros.base.IMacro;
import healthcare.powerfullspanstesting.macros.base.BaseTimeMacro;

public class MainActivity extends AppCompatActivity {

    EditText mEditor;
    private Button mButton;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = this;

        //Exception on textview click http://stackoverflow.com/questions/33821008/illegalargumentexception-while-selecting-text-in-android-textview
        mEditor = (EditText) findViewById(R.id.editor);
        mEditor.setMovementMethod(LinkMovementMethod.getInstance());



        mButton = (Button) findViewById(R.id.insert_button);
        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CheckboxListMacro span = new CheckboxListMacro(mContext, "Операция",
                        new ListDialogItem(new TextMacro("Test 1")),
                        new ListDialogItem(new TextMacro("Test 2")),
                        new ListDialogItem(new TextMacro("Test 3")),
                        new ListDialogItem(new TextMacro("Test 4")),
                        new ListDialogItem(new TextMacro("Test 5")),
                        new ListDialogItem(new TextMacro("Test 6")),
                        new ListDialogItem(new NumberMacro(mContext, "Начало")),
                        new ListDialogItem(new NumberMacro(mContext, "Окончание")),
                        new ListDialogItem(new CheckboxListMacro(mContext, "Хирурги",
                                new ListDialogItem(new TextMacro("Иванов")),
                                new ListDialogItem(new TextMacro("Петров")))),
                        new ListDialogItem(new DateMacro(mContext, "Дата операции")),
                        new ListDialogItem(new TimeMacro(mContext, "Время приема")),
                        new ListDialogItem(new DateTimeMacro(mContext, "Начало осмотра"))
                        );

                span.setUpdateSpansStateListener(new TextMacro.UpdateSpansStateEvent() {
                    @Override
                    public void Add(Object span, SpannableString spannable) {
                        int start = mEditor.getText().getSpanStart(span);
                        int end = mEditor.getText().getSpanEnd(span);

                        mEditor.getText().insert(end, spannable);
                        mEditor.setSelection(end + spannable.length());
                    }

                    @Override
                    public void Update(Object macros, SpannableString spannable) {
                        int start = mEditor.getText().getSpanStart(macros);
                        int end = mEditor.getText().getSpanEnd(macros);

                        mEditor.getText().replace(start, end,
                                spannable, 0, spannable.length());
                    }

                    @Override
                    public void remove(IMacro macro) {
                        int start = mEditor.getText().getSpanStart(macro);
                        int end = mEditor.getText().getSpanEnd(macro);

                        mEditor.getText().replace(start, end, "");
                    }

                });
                SpannableString ss = span.getSpannableString();
                int start = Math.max(mEditor.getSelectionStart(), 0);
                int end = Math.max(mEditor.getSelectionEnd(), 0);
                mEditor.getText().replace(Math.min(start, end), Math.max(start, end),
                        ss, 0, ss.length());
            }
        });
    }





//        mButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                SpannableString ss = new SpannableString("abc");
//                Drawable d = getResources().getDrawable(R.drawable.dots);
//                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
//                ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
//                ss.setSpan(span, 0, 3, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
////                String text = "macros";
////                Spannable span = new SpannableString(text);
////                span.setSpan(new UnderlineSpan(), 0, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//                int start = Math.max(mEditor.getSelectionStart(), 0);
//                int end = Math.max(mEditor.getSelectionEnd(), 0);
//                mEditor.getText().replace(Math.min(start, end), Math.max(start, end),
//                        ss, 0, ss.length());
//            }
//        });


}
