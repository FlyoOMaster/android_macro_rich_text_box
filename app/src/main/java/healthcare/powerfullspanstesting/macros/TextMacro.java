package healthcare.powerfullspanstesting.macros;

import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.CharacterStyle;
import android.text.style.ClickableSpan;
import android.view.View;

import java.util.ArrayList;

import healthcare.powerfullspanstesting.macros.base.IMacro;
import healthcare.powerfullspanstesting.macros.base.IMacroParent;
import healthcare.powerfullspanstesting.macros.spans.ChildStyleSpan;
import healthcare.powerfullspanstesting.macros.spans.SpanInfo;

/**
 * Created by trubnikov_ya on 07.04.2017.
 */

/**
 * Текстовый макрос
 */
public class TextMacro extends CharacterStyle implements IMacro {

    //region Private fields
    private IMacroParent mParent;
    //endregion

    //region Protected fields
    protected String mText;
    protected final String mSpace = " ";
    protected SpannableString mSpannableString;
    protected ClickableSpan mTextClickableSpan;
    protected ArrayList<SpanInfo> mLabelSpansInfo = new ArrayList<>();

    protected UpdateSpansStateEvent mUpdateSpansStateListener;
    //endregion

    //region Public events

    /**
     * События макросов
     */
    public interface UpdateSpansStateEvent {
        /**
         * Вызвается, когда необходимо добавить новый макрос
         * @param span после какого макроса произвести вставку
         * @param spannable
         */
        void Add(Object span, SpannableString spannable);

        /**
         * Вызвается, когда надо обновить указанный макрос
         * @param macros макрос
         * @param spannable новый SpannableString макроса
         */
        void Update(Object macros, SpannableString spannable);

        /**
         * Вызывается, когда нужно удалить указанный макрос
         * @param macro макрос
         */
        void remove(IMacro macro);
    }
    //endregion

    //region Public constructors
    public TextMacro(String text) {
        init(text);
    }
    //endregion

    //region Protected methods

    /**
     * Установка макроса на label
     * @param spanInfo SpanInfo заголовка
     */
    protected void setTextSpan(SpanInfo spanInfo) {
        mSpannableString.setSpan(spanInfo.getType(), spanInfo.getStart(), spanInfo.getEnd(), spanInfo.getFlags());
        spanInfo.setSpannable(mSpannableString);
    }
    //endregion

    //region Public methods

    /**
     * Изменяет текст label
     * @param value
     */
    public void changeLabelValues(String value){
        mText = value;

        generateLabelSpannableString();

        for (SpanInfo span : getLabelSpansInfo()) {
            setTextSpan(span);
        }

        update();
    }

    @Override
    public void updateDrawState(TextPaint tp) {
        // empty
    }

    @Override
    public void setParent(final IMacroParent parent) {
        if (mParent == parent)
        {
            return;
        }

        mParent = parent;

        if (parent != null)
        {
            mTextClickableSpan = new ChildStyleSpan() {
                @Override
                public void onClick(View widget) {
                    parent.activate();
                }
            };

            addSpanToText(mTextClickableSpan);
        }

    }

    @Override
    public String getLabel() {
        return mText;
    }

    /**
     * Добавляет к стилизации label доп макрос
     * @param span стиль/маркос
     */
    public void addSpanToText(Object span) {
        SpanInfo spanInfo = new SpanInfo(span, 0, mText.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        setTextSpan(spanInfo);
        mLabelSpansInfo.add(spanInfo);
    }

    public SpannableString getSpannableString()
    {
        return mSpannableString;
    }

    @Override
    public void setUpdateSpansStateListener(UpdateSpansStateEvent listener) {
        mUpdateSpansStateListener = listener;
    }

    @Override
    public ArrayList<SpanInfo> getLabelSpansInfo() {
        return mLabelSpansInfo;
    }

    @Override
    public void reset() {
        mParent = null;
        mLabelSpansInfo.clear();
        init(mText);
    }

    private void init(String text) {
        mText = text;
        generateLabelSpannableString();
    }

    /**
     * Удаляет все макросы label
     * @param macro
     */
    public void removeLabelSpansInfo(IMacro macro) {
        for (int i = 0; i < mLabelSpansInfo.size(); i++) {
            if (mLabelSpansInfo.get(i) == macro)
            {
                mLabelSpansInfo.remove(i);
            }
        }
    }

    @Override
    public void dismiss() {
        mParent = null;
        mLabelSpansInfo.clear();
        mTextClickableSpan = null;
    }
    //endregion

    //region Private methods
    private void generateLabelSpannableString() {
        mSpannableString = new SpannableString(mText + mSpace);
        mSpannableString.setSpan(this, 0, mSpannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
    }

    private void update() {
        if (mUpdateSpansStateListener != null) {
            mUpdateSpansStateListener.Update(this, mSpannableString);
        }
    }
    //endregion
}
