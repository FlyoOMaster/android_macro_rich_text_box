package healthcare.powerfullspanstesting.macros.base;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.view.View;

import java.util.ArrayList;

import healthcare.powerfullspanstesting.R;
import healthcare.powerfullspanstesting.dialogs.base.BaseMacroDialog;
import healthcare.powerfullspanstesting.dialogs.base.IMacroDialog;
import healthcare.powerfullspanstesting.macros.TextMacro;
import healthcare.powerfullspanstesting.macros.spans.ChildStyleSpan;
import healthcare.powerfullspanstesting.macros.spans.MacroImageSpan;
import healthcare.powerfullspanstesting.macros.spans.ParentStyleSpan;
import healthcare.powerfullspanstesting.macros.spans.SpanHelper;
import healthcare.powerfullspanstesting.macros.spans.SpanInfo;

/**
 * Created by trubnikov_ya on 06.04.2017.
 */

/**
 * Базовый класс для макроса с кнопкой
 * @param <TResult> Возвращаемый тип в результате работы диалогового окна
 */
public abstract class BaseMacroButton<TResult> extends TextMacro implements IMacroParent {

    //region Private fields
    private MacroImageSpan mImageSpan;
    private ChildStyleSpan mClickableSpan;
    private ArrayList<SpanInfo> mButtonSpansInfo = new ArrayList<>();
    private ParentStyleSpan mTextParentSpan;
    //endregion

    //region Protected fields
    protected Context mContext;
    protected String mButton;
    protected Drawable mDrawable;
    protected ArrayList<IMacro> mActiveMacros = new ArrayList<>();
    //endregion

    //region Public constructors

    public BaseMacroButton(Context context, String label) {
        super(label);
        initializeButtonMacro(context, R.drawable.dots);
    }

    public BaseMacroButton(Context context, String label, Drawable drawable) {
        super(label);
        initializeButtonMacro(context, drawable);
    }
    //endregion

    //region Public methods

    @Override
    public void reset() {
        super.reset();
        ArrayList<IMacro> macroChilds = getActiveMacros();

        for (IMacro macroChild : macroChilds) {
            macroChild.reset();

            if (mUpdateSpansStateListener != null) {
                mUpdateSpansStateListener.remove(macroChild);
            }
        }


        mActiveMacros.clear();
        initializeButtonMacro(mContext, mDrawable);
    }

    public void activate() {
        final BaseMacroDialog<TResult> dialog = getDialogView();
        dialog.setOnClickListener(new IMacroDialog.DialogClickEvent<TResult>() {
            @Override
            public void onOk(TResult value) {
                changeButtonValue("");
                removeSpanFromButton(mImageSpan);
                expandMacro(value);
                update();
            }

            @Override
            public void onCancel() {

            }
        });

        dialog.show();
    }

    public void setMacrosButton(int id) {
        Drawable drawable = ResourcesCompat.getDrawable(mContext.getResources(), id, null);

        setMacrosButton(drawable);
    }

    public void setMacrosButton(Drawable drawable) {
        if (drawable != null) {
            mDrawable = drawable;
            mDrawable.setBounds(0, 0, mDrawable.getIntrinsicWidth(), mDrawable.getIntrinsicHeight());
        }
    }

    public SpannableString getSpannableString() {
        return mSpannableString;
    }

    @Override
    public void updateDrawState(TextPaint tp) {

    }

    @Override
    public ArrayList<IMacro> getActiveMacros() {
        return mActiveMacros;
    }

    public void dismiss() {
        super.dismiss();
        mUpdateSpansStateListener = null;
        mTextParentSpan = null;
    }

    @Override
    public void setParent(IMacroParent parent) {
        super.setParent(parent);

        mTextParentSpan = new ParentStyleSpan() {
            @Override
            public void onClick(View widget) {
                mTextClickableSpan.onClick(widget);
            }
        };

        addSpanToText(mTextParentSpan);
    }

    @Override
    public IMacro getLastChild() {
        if (getActiveMacros().size() == 0) {
            return this;
        }
        return getActiveMacros().get(getActiveMacros().size() - 1);
    }

    //endregion

    //region Protected methods

    /**
     * Добавить дочерний макрос
     * @param macroToAppend макрос, после которого добавить
     * @param macro добавляемый макрос
     */
    protected void addMacro(IMacro macroToAppend, IMacro macro) {
        macro.setParent(this);

        if (mUpdateSpansStateListener != null) {
            mUpdateSpansStateListener.Add(macroToAppend, macro.getSpannableString());
        }

        mActiveMacros.add(macro);
    }

    /**
     * Удалить все активные макросы
     */
    protected void removeAllActiveSpans() {
        for (IMacro macro : mActiveMacros) {
            removeMacro(macro);
        }
    }

    /**
     * Удалить указанный макрос
     * @param macro макрос для удаления
     */
    protected void removeMacro(IMacro macro) {
        try {
            if (mActiveMacros.contains(macro)) {
                macro.reset();

                if (mUpdateSpansStateListener != null) {
                    mUpdateSpansStateListener.remove(macro);
                }

                mActiveMacros.remove(macro);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Добавить макпрос к  кнопке
     * @param macro макрос / стиль
     */
    protected void addSpanToButton(Object macro) {
        SpanInfo span = new SpanInfo(macro);
        setButtonSpan(span);
        mButtonSpansInfo.add(span);
    }

    protected abstract BaseMacroDialog<TResult> getDialogView();

    /**
     * Действия, производимые после получения данных от диалогового окна
     * @param value результат работы диалогового окна
     */
    protected abstract void expandMacro(TResult value);

    /**
     * Изменить текст кнопки
     * @param value текст
     */
    protected void changeButtonValue(String value) {
        mButton = value;

        if (isAdditionalSpaceNeeded()) {
            generateSpannableWithAdditionalSpace();
        } else {
            generateSpannableWithoutAdditionalSpace();
        }


        for (SpanInfo span : getLabelSpansInfo()) {
            setTextSpan(span);
        }

        for (SpanInfo span : mButtonSpansInfo) {
            setButtonSpan(span);
        }

        update();
    }

    protected void initializeButtonMacro(Context context, int id) {
        initializeButtonMacro(context, ResourcesCompat.getDrawable(context.getResources(), id, null));
    }

    protected void initializeButtonMacro(Context context, Drawable drawable) {
        mContext = context;

        if (mButton == null || TextUtils.isEmpty(mButton))
        {
            mButton = "btn";
        }

        generateSpannableWithAdditionalSpace();
        mSpannableString.setSpan(this, 0, mSpannableString.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);



        mClickableSpan = new ChildStyleSpan() {
            @Override
            public void onClick(View widget) {
                activate();
            }
        };
        addSpanToButton(mClickableSpan);

        setMacrosButton(drawable);

        if (mDrawable != null) {
            mImageSpan = new MacroImageSpan(mDrawable);
            addSpanToButton(mImageSpan);
        }
    }

    /**
     * Обновить состояние макроса
     */
    protected void update() {
        update(BaseMacroButton.this, mSpannableString);
    }

    //endregion

    //region Private methods

    private void generateSpannableWithAdditionalSpace() {
        mSpannableString = new SpannableString(mText + mSpace + mButton + mSpace);
    }

    private void generateSpannableWithoutAdditionalSpace() {
        mSpannableString = new SpannableString(mText + mSpace + mButton);
    }

    /**
     * Удалить макрос/стиль кнопки
     * @param span макрос/стиль
     */
    private void removeSpanFromButton(Object span) {
        SpanInfo spanInfo = SpanHelper.findSpan(mButtonSpansInfo, span);
        if (spanInfo != null) {
            mSpannableString.removeSpan(span);
            mButtonSpansInfo.remove(spanInfo);
        }
    }

    /**
     * Установить макрос/стиль для кнопки
     * @param span макрос/стиль
     */
    private void setButtonSpan(SpanInfo span) {
        if (isAdditionalSpaceNeeded()) {
            span.setStart(mSpannableString.length() - mButton.length() - mSpace.length());
            span.setEnd(mSpannableString.length() - mSpace.length());
        } else {
            span.setStart(mSpannableString.length() - mButton.length());
            span.setEnd(mSpannableString.length());
        }
        mSpannableString.setSpan(span.getType(), span.getStart(), span.getEnd(), span.getFlags());
        span.setSpannable(mSpannableString);

    }

    private void update(BaseMacroButton span, SpannableString spannable) {
        if (mUpdateSpansStateListener != null) {
            mUpdateSpansStateListener.Update(span, spannable);
        }
    }

    private boolean isAdditionalSpaceNeeded() {
        return !TextUtils.isEmpty(mButton);
    }
    //endregion
}
