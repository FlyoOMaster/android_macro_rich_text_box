package healthcare.powerfullspanstesting.macros.base;

import java.util.ArrayList;

/**
 * Created by trubnikov_ya on 10.04.2017.
 */

public interface IMacroParent extends IMacro {
    /**
     * Действия, производимые после нажатия на кнопку
     */
    void activate();

    /**
     * Получить список активных макросов
     * @return Список активных макросов
     */
    ArrayList<IMacro> getActiveMacros();

    /**
     * Получить последний макрос из своей коллекции макросов
     * @return Последний дочерний макрос
     */
    IMacro getLastChild();
}
