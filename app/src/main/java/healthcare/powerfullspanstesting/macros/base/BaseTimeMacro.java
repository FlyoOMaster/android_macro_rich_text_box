package healthcare.powerfullspanstesting.macros.base;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import healthcare.powerfullspanstesting.macros.TextMacro;
import healthcare.powerfullspanstesting.macros.spans.DateStyleSpan;
import healthcare.powerfullspanstesting.macros.spans.SpanInfo;

/**
 * Created by trubnikov_ya on 12.04.2017.
 */

/**
 * Базовый класс для макроса со временем
 */
public abstract class BaseTimeMacro extends BaseMacroButton<Date> implements IMacroParent {

    //region Protected fields
    protected Calendar mToday = Calendar.getInstance();
    //endregion

    //region Public methods
    public BaseTimeMacro(Context context, String label) {
        super(context, label, null);
        reset();
    }
    //endregion

    //region Public methods

    @Override
    public void reset() {
        mButton = getTimeFormat().format(mToday.getTime());
        super.reset();
        addSpanToButton(new DateStyleSpan(Color.parseColor("#388186")));
    }

    //endregion

    //region Protected methods

    /**
     * Установить время
     * @param time
     */
    protected void setTime(Date time) {
        removeAllActiveSpans();

        TextMacro dateMacro = new TextMacro(getTimeFormat().format(time));
        dateMacro.addSpanToText(new DateStyleSpan(Color.parseColor("#388186")));
        addMacro(this, dateMacro);
    }

    protected abstract SimpleDateFormat getTimeFormat();

    @Override
    protected void expandMacro(Date value) {
        if (value == null)
        {
            return;
        }

        setTime(value);
    }
    //endregion
}
