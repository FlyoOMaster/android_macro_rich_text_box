package healthcare.powerfullspanstesting.macros.base;

import android.content.Context;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.Arrays;

import healthcare.powerfullspanstesting.dialogs.base.BaseMacroDialog;
import healthcare.powerfullspanstesting.dialogs.collection.ListDialogItem;

/**
 * Created by trubnikov_ya on 07.04.2017.
 */

/**
 * Базовый класс макроса со списком
 */
public abstract class BaseListMacro<TResult extends ListDialogItem> extends BaseMacroButton<ArrayList<TResult>> {

    //region Protected fields
    protected ArrayList<TResult> mItems;
    //endregion

    //region Public constructors
    public BaseListMacro(Context context, String label, TResult... items) {
        super(context, label);
        init(items);
    }

    public BaseListMacro(Context context, String label, Drawable drawable) {
        super(context, label, drawable);
    }
    //endregion

    //region Public methods

    @Override
    public void reset() {
        super.reset();
        init(mItems);
    }



    @Override
    public void dismiss() {
        super.dismiss();

        for (IMacro macro : mActiveMacros) {
            macro.dismiss();
        }

        for (ListDialogItem item : mItems) {
            item.macro.dismiss();
        }

        super.dismiss();
    }

    //endregion

    //region Protected methods
    @Override
    protected void expandMacro(ArrayList<TResult> value) {
        if (isSomethingChecked()) {
            clearUncheckedItems();
            insertCheckedItems();
        } else {
            reinitialize();
        }
    }

    protected abstract BaseMacroDialog<ArrayList<TResult>> getDialogView();
    //endregion

    //region Private methods
    private void init(TResult[] items) {
        mItems = new ArrayList<>(Arrays.asList(items));
    }

    private void init(ArrayList<TResult> items) {
        mActiveMacros = new ArrayList<>();
        mItems = items;

        for (ListDialogItem item : mItems) {
            item.isSelected = false;
        }
    }

    /**
     * Добавить выделеные макросы
     */
    private void insertCheckedItems() {
        boolean isFirstMacro = mActiveMacros.size() == 0;
        for (ListDialogItem item : mItems) {
            if (item.isSelected) {
                if (!mActiveMacros.contains(item.macro)) {
                    final IMacro macro = item.macro;
                    macro.setUpdateSpansStateListener(mUpdateSpansStateListener);

                    if (isFirstMacro) {
                        isFirstMacro = false;
                        changeButtonValue("");
                        addMacro(this, macro);
                    } else {

                        IMacro macroToAppend = getMacroToAppend(item);
                        addMacro(macroToAppend, macro);

                    }
                }
            }
        }
    }

    /**
     * Удалить все дочерние макросы, выделение которых было снято
     */
    private void clearUncheckedItems() {
        for (ListDialogItem item : mItems) {
            if (mActiveMacros.contains(item.macro) && !item.isSelected) {
                removeMacro(item.macro);
            }
        }
    }

    private void reinitialize() {
        for (ListDialogItem item : mItems) {
            removeMacro(item.macro);
        }
        initializeButtonMacro(mContext, mDrawable);

        if (mUpdateSpansStateListener != null) {
            mUpdateSpansStateListener.Update(this, this.getSpannableString());
        }

        mActiveMacros.clear();
    }

    /**
     * Имеются ли хотябы один выбранный макрос
     * @return выбран ли хоть один элемент
     */
    private boolean isSomethingChecked() {
        for (int i = 0; i < mItems.size(); i++) {
            if (mItems.get(i).isSelected) return true;
        }

        return false;
    }

    /**
     * Получить макрос после которого будет вставлен указанный макрос
     * @param insertingItem макрос для вставки
     * @return макрос, после которого произойдет вставка
     */
    private IMacro getMacroToAppend(ListDialogItem insertingItem) {
        int insertingMacroIndex = mItems.indexOf(insertingItem);

        if (insertingMacroIndex == 0) {
            return this;
        }

        for (int i = insertingMacroIndex - 1; i >= 0; i--) {
            IMacro currentMacro = mItems.get(i).macro;
            if (mActiveMacros.contains(currentMacro)) {
                if (currentMacro instanceof IMacroParent) {
                    IMacroParent macroParent = ((IMacroParent) currentMacro);
                    return macroParent.getLastChild();
                } else {
                    return currentMacro;
                }
            }

            if (i == 0 && !mActiveMacros.contains(currentMacro)) {
                return this;
            }
        }

        return mActiveMacros.get(mActiveMacros.size() - 1);
    }
    //endregion

}
