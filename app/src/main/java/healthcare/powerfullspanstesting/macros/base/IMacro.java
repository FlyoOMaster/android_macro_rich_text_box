package healthcare.powerfullspanstesting.macros.base;

import android.text.SpannableString;

import java.util.ArrayList;

import healthcare.powerfullspanstesting.macros.TextMacro;
import healthcare.powerfullspanstesting.macros.spans.SpanInfo;

/**
 * Created by trubnikov_ya on 07.04.2017.
 */

public interface IMacro {
    /**
     * Освободить ресурсы
     */
    void dismiss();

    /**
     * Сбросить до начального состояния
     */
    void reset();

    /**
     * Установить родителя
     * @param parent родительский макрос
     */
    void setParent(IMacroParent parent);

    /**
     * Установить обработчик событий макроса
     * @param listener оброботчик событий макроса
     */
    void setUpdateSpansStateListener(TextMacro.UpdateSpansStateEvent listener);

    /**
     * Получить label макроса
     * @return заголовок макроса
     */
    String getLabel();

    /**
     * Получить все SpanInfo label
     * @return список примененных стилей/макросов к заголовку
     */
    ArrayList<SpanInfo> getLabelSpansInfo();

    /**
     * Получить SpannableString label
     * @return SpannableString заголовка
     */
    SpannableString getSpannableString();
}
