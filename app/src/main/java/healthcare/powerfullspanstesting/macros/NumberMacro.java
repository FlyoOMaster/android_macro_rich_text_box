package healthcare.powerfullspanstesting.macros;

import android.content.Context;
import android.graphics.drawable.Drawable;

import healthcare.powerfullspanstesting.dialogs.NumberDialog;
import healthcare.powerfullspanstesting.dialogs.base.BaseMacroDialog;
import healthcare.powerfullspanstesting.macros.base.BaseMacroButton;
import healthcare.powerfullspanstesting.macros.spans.ChildStyleSpan;

/**
 * Created by trubnikov_ya on 07.04.2017.
 */

/**
 * Макрос число
 */
public class NumberMacro extends BaseMacroButton<Integer> {

    //region Public constructors
    public NumberMacro(Context context, String label) {
        super(context, label);
    }

    public NumberMacro(Context context, String label, Drawable drawable) {
        super(context, label, drawable);
    }
    //endregion

    //region Protected methods
    @Override
    protected BaseMacroDialog<Integer> getDialogView() {
        return new NumberDialog(mContext);
    }

    @Override
    protected void expandMacro(Integer value) {
        if (value == null) {
            return;
        }

        removeAllActiveSpans();

        TextMacro macroText = new TextMacro(String.valueOf(value));
        addMacro(this, macroText);
    }
    //endregion
}
