package healthcare.powerfullspanstesting.macros;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import healthcare.powerfullspanstesting.dialogs.DateDialog;
import healthcare.powerfullspanstesting.dialogs.base.BaseMacroDialog;
import healthcare.powerfullspanstesting.macros.base.BaseTimeMacro;
import healthcare.powerfullspanstesting.macros.base.IMacroParent;
import healthcare.powerfullspanstesting.macros.spans.DateStyleSpan;

/**
 * Created by trubnikov_ya on 12.04.2017.
 */

/**
 * Макрос-дата
 */
public class DateMacro extends BaseTimeMacro implements IMacroParent {

    //region Public constructors
    public DateMacro(Context context, String label) {
        super(context, label);
    }
    //endregion

    //region Protected methods
    @Override
    protected SimpleDateFormat getTimeFormat() {
        return new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
    }

    @Override
    protected BaseMacroDialog<Date> getDialogView() {
        return new DateDialog(mContext);
    }
    //endregion
}
