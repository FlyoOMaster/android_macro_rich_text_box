package healthcare.powerfullspanstesting.macros.spans;

import android.text.SpannableString;
import android.text.Spanned;

/**
 * Created by trubnikov_ya on 10.04.2017.
 */

public class SpanInfo {
    //region Private fields
    private int mStart;
    private int mEnd;
    private int mFlags = Spanned.SPAN_EXCLUSIVE_EXCLUSIVE;
    private Object mType;
    private SpannableString mSpannable;
    //endregion

    //region Public constructors
    public SpanInfo(Object type) {
        this.mType = type;
    }

    public SpanInfo(Object type, int start, int end, int flags) {
        this.mStart = start;
        this.mEnd = end;
        this.mFlags = flags;
        this.mType = type;
    }
    //endregion

    //region Public properties
    public void setStart(int mStart) {
        this.mStart = mStart;
    }

    public int getStart() {
        if (mSpannable != null)
        {
            //return mSpannable.getSpanStart(mType);
        }

        return mStart;
    }

    public void setEnd(int mEnd) {
        this.mEnd = mEnd;
    }

    public int getEnd() {
        if (mSpannable != null)
        {
            //return mSpannable.getSpanEnd(mType);
        }

        return mEnd;
    }

    public void setFlags(int mFlags) {
        this.mFlags = mFlags;
    }

    public int getFlags() {
        return mFlags;
    }

    public void setType(Object mType) {
        this.mType = mType;
    }

    public Object getType() {
        return mType;
    }

    public SpannableString getSpannable() {
        return mSpannable;
    }

    public void setSpannable(SpannableString mSpannable) {
        this.mSpannable = mSpannable;
    }
    //endregion
}
