package healthcare.powerfullspanstesting.macros.spans;

import android.graphics.Color;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

/**
 * Created by trubnikov_ya on 11.04.2017.
 */

public abstract class ParentStyleSpan extends ClickableSpan {

    public abstract void onClick(View widget);

    @Override
    public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setColor(Color.parseColor("#388186"));
    }
}