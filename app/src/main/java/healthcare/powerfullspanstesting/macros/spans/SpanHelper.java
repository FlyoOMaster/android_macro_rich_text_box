package healthcare.powerfullspanstesting.macros.spans;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.ArrayList;

/**
 * Created by trubnikov_ya on 10.04.2017.
 */

public class SpanHelper {
    @Nullable
    public static SpanInfo findSpan(@NonNull ArrayList<SpanInfo> spansInfo, @NonNull Object span) {
        for (SpanInfo s : spansInfo) {
            if (s.getType() == span) {
                return s;
            }
        }

        return null;
    }
}
