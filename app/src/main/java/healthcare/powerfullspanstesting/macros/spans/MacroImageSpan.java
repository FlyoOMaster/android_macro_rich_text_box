package healthcare.powerfullspanstesting.macros.spans;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.text.style.ImageSpan;

/**
 * Created by trubnikov_ya on 06.04.2017.
 */

public class MacroImageSpan extends ImageSpan {
    public MacroImageSpan(Drawable d) {
        super(d);
    }

    @Override
    public void draw(Canvas canvas, CharSequence text, int start, int end, float x, int top, int y, int bottom, Paint paint) {
        canvas.save();

        float transY = bottom - (getDrawable().getBounds().bottom / 2);
        Paint.FontMetricsInt fm = paint.getFontMetricsInt();
        transY -= (fm.descent - fm.ascent) / 2;

        canvas.translate(x, transY);
        getDrawable().draw(canvas);
        canvas.restore();
    }
}
