package healthcare.powerfullspanstesting.macros.spans;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.style.DynamicDrawableSpan;
import android.text.style.ReplacementSpan;

/**
 * Created by trubnikov_ya on 12.04.2017.
 */

public class DateStyleSpan extends ReplacementSpan {
    private final int mTextColor;
    private int mWidth;

    public DateStyleSpan(int textColor) {
        mTextColor = textColor;
    }

    @Override
    public int getSize(@NonNull Paint paint, CharSequence text, @IntRange(from = 0) int start, @IntRange(from = 0) int end, @Nullable Paint.FontMetricsInt fm) {
        mWidth = (int) paint.measureText(text, start, end);
        return mWidth;
    }

    @Override
    public void draw(@NonNull Canvas canvas, CharSequence text, @IntRange(from = 0) int start, @IntRange(from = 0) int end, float x, int top, int y, int bottom, @NonNull Paint paint) {
        int offset = 5;
        Paint.FontMetricsInt fm = paint.getFontMetricsInt();
        Rect r = new Rect((int)x, Math.round(y + fm.ascent), Math.round(x + paint.measureText(text, start, end)), y + fm.descent);
        r.left -= offset;
        r.top -= offset;
        r.right += offset;
        r.bottom += offset;

        Paint myPaint = new Paint();
        myPaint.setColor(Color.rgb(0, 0, 0));
        myPaint.setStyle(Paint.Style.STROKE);
        myPaint.setStrokeWidth(1);

        paint.setColor(mTextColor);
        canvas.drawText(text, start, end, x, y, paint);
        canvas.drawRect(r, myPaint);
    }
}
