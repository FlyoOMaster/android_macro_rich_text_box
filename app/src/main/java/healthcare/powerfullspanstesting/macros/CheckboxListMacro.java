package healthcare.powerfullspanstesting.macros;

import android.content.Context;
import android.graphics.drawable.Drawable;

import java.util.ArrayList;
import java.util.Arrays;

import healthcare.powerfullspanstesting.dialogs.ListDialog;
import healthcare.powerfullspanstesting.dialogs.base.BaseMacroDialog;
import healthcare.powerfullspanstesting.dialogs.collection.ListDialogItem;
import healthcare.powerfullspanstesting.macros.base.BaseListMacro;
import healthcare.powerfullspanstesting.macros.base.BaseMacroButton;
import healthcare.powerfullspanstesting.macros.base.IMacro;
import healthcare.powerfullspanstesting.macros.base.IMacroParent;

/**
 * Created by trubnikov_ya on 07.04.2017.
 */

/**
 * Макрос-спсиок с множественным выбором
 */
public class CheckboxListMacro extends BaseListMacro<ListDialogItem> implements IMacroParent {

    //region Public constructros
    public CheckboxListMacro(Context context, String label, ListDialogItem... items) {
        super(context, label, items);
    }

    public CheckboxListMacro(Context context, String label, Drawable drawable) {
        super(context, label, drawable);
    }
    //endregion

    //region Protected methods
    @Override
    protected BaseMacroDialog<ArrayList<ListDialogItem>> getDialogView() {
        return new ListDialog(mContext, mItems, true);
    }
    //endregion
}
