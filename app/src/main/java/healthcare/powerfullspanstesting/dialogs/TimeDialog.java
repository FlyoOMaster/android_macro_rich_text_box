package healthcare.powerfullspanstesting.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import healthcare.powerfullspanstesting.R;
import healthcare.powerfullspanstesting.dialogs.base.BaseMacroDialog;

/**
 * Created by trubnikov_ya on 12.04.2017.
 */

public class TimeDialog extends BaseMacroDialog<Date> implements TimePicker.OnTimeChangedListener {

    //region Private fields
    private final TimePicker mTimePicker;
    private Calendar mTime;
    //endregion

    //region Public constructors
    public TimeDialog(@NonNull Context context) {
        super(context);
        mTime = Calendar.getInstance();
        mTimePicker = (TimePicker) findViewById(R.id.timePicker);
        mTimePicker.setIs24HourView(true);
        mTimePicker.setOnTimeChangedListener(this);
    }
    //endregion

    //region Public methods
    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        mTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mTime.set(Calendar.MINUTE, minute);
    }
    //endregion

    //region Protected methods
    @Override
    protected int getDialogView() {
        return R.layout.macro_time_dialog;
    }

    @Override
    protected Date doOnOkClick() {
        return  mTime.getTime();
    }

    @Override
    protected void doOnCancelClick() {

    }

    @Override
    protected void doOnPreShow() {

    }
    //endregion
}
