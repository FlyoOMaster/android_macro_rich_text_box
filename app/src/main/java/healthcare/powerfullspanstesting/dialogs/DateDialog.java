package healthcare.powerfullspanstesting.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import healthcare.powerfullspanstesting.R;
import healthcare.powerfullspanstesting.dialogs.base.BaseMacroDialog;

/**
 * Created by trubnikov_ya on 12.04.2017.
 */

public class DateDialog extends BaseMacroDialog<Date> implements DatePicker.OnDateChangedListener {

    //region Private fields
    private final DatePicker mDatePicker;
    private Calendar mDate;
    //endregion

    //region Public constructors
    public DateDialog(@NonNull Context context) {
        super(context);
        mDate = Calendar.getInstance();
        mDatePicker = (DatePicker) findViewById(R.id.datePicker);
        mDatePicker.init(mDate.get(Calendar.YEAR), mDate.get(Calendar.MONTH),
                mDate.get(Calendar.DAY_OF_MONTH), this);
    }
    //endregion

    //region Protected methods
    @Override
    protected int getDialogView() {
        return R.layout.macro_date_dialog;
    }

    @Override
    protected Date doOnOkClick() {
        return mDate.getTime();
    }

    @Override
    protected void doOnCancelClick() {

    }

    @Override
    protected void doOnPreShow() {

    }
    //endregion

    //region Public methods
    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        mDate = Calendar.getInstance();
        mDate.set(year, monthOfYear, dayOfMonth);
    }
    //endregion
}
