package healthcare.powerfullspanstesting.dialogs.collection;

import android.widget.TextView;

import healthcare.powerfullspanstesting.macros.TextMacro;
import healthcare.powerfullspanstesting.macros.base.IMacro;

/**
 * Created by trubnikov_ya on 07.04.2017.
 */

public class ListDialogItem {
    //region Public fields
    public String label;
    public IMacro macro;
    public boolean isSelected;
    //endregion

    //region Public constructors
    public ListDialogItem(String label, IMacro value) {
        init(label, value);
    }

    public ListDialogItem(TextMacro macro) {
        init(macro.getLabel(), macro);
    }
    //endregion

    //region Private methods
    private void init(String label, IMacro value) {
        this.label = label;
        this.macro = value;
    }
    //endregion
}
