package healthcare.powerfullspanstesting.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import healthcare.powerfullspanstesting.R;
import healthcare.powerfullspanstesting.dialogs.base.BaseMacroDialog;

/**
 * Created by trubnikov_ya on 12.04.2017.
 */

public class DateTimeDialog extends BaseMacroDialog<Date> implements TimePicker.OnTimeChangedListener, DatePicker.OnDateChangedListener {

    //region Private fileds
    private DatePicker mDatePicker;
    private TimePicker mTimePicker;
    private Calendar mDateTime;
    private boolean isDateVisible = true;
    //endregion

    //region Public constructors
    public DateTimeDialog(@NonNull Context context) {
        super(context);
        mDateTime = Calendar.getInstance();
        mDatePicker = (DatePicker) findViewById(R.id.datePicker);
        mDatePicker.init(mDateTime.get(Calendar.YEAR), mDateTime.get(Calendar.MONTH),
                mDateTime.get(Calendar.DAY_OF_MONTH), this);

        mTimePicker = (TimePicker) findViewById(R.id.timePicker);
        mTimePicker.setIs24HourView(true);
        mTimePicker.setOnTimeChangedListener(this);
    }
    //endregion

    //region Protected methods
    @Override
    protected int getDialogView() {
        return R.layout.macro_date_time_dialog;
    }

    @Override
    protected Date doOnOkClick() {
        toggleView();

        return mDateTime.getTime();
    }

    @Override
    protected void doOnCancelClick() {

    }

    @Override
    protected void doOnPreShow() {

    }
    //endregion

    //region Public methods
    @Override
    public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        mDateTime.set(Calendar.DAY_OF_YEAR, year);
        mDateTime.set(Calendar.MONTH, monthOfYear);
        mDateTime.set(Calendar.DAY_OF_YEAR, dayOfMonth);

        toggleView();
    }


    @Override
    public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
        mDateTime.set(Calendar.HOUR_OF_DAY, hourOfDay);
        mDateTime.set(Calendar.MINUTE, minute);
    }
    //endregion

    //region Private methods
    private void toggleView() {
        isDateVisible = !isDateVisible;

        if (isDateVisible)
        {
            mDatePicker.setVisibility(View.VISIBLE);
            mTimePicker.setVisibility(View.GONE);
        } else {
            mDatePicker.setVisibility(View.GONE);
            mTimePicker.setVisibility(View.VISIBLE);
        }
    }
    //endregion
}
