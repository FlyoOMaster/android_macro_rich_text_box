package healthcare.powerfullspanstesting.dialogs.base;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;

import healthcare.powerfullspanstesting.R;

/**
 * Created by trubnikov_ya on 07.04.2017.
 * Базовый класс диалоговых окон
 */

public abstract class BaseMacroDialog<T> extends Dialog implements IMacroDialog<T>, DialogInterface.OnDismissListener {

    //region Private fields
    private DialogClickEvent<T> mClickListener;
    //endregion

    //region Protected fields
    protected Context mContext;
    //endregion

    //region Public constructor
    public BaseMacroDialog(@NonNull Context context) {
        super(context);
        initializeMacro(context);
    }
    //endregion

    //region Private methods
    private void initializeMacro(Context context) {
        mContext = context;
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setCancelable(true);
        setContentView(getDialogView());
        getDialogOkButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) {
                    mClickListener.onOk(doOnOkClick());
                }

                dismiss();
            }
        });

        getDialogCancelButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) {
                    doOnCancelClick();
                    mClickListener.onCancel();
                }

                dismiss();
            }
        });

        setOnDismissListener(this);
    }
    //endregion

    //region Public methods
    @Override
    public void setOnClickListener(DialogClickEvent<T> listener) {
        mClickListener = listener;
    }

    @Override
    public void show() {
        doOnPreShow();
        super.show();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        setOnDismissListener(null);
        getDialogOkButton().setOnClickListener(null);
        getDialogCancelButton().setOnClickListener(null);
        mClickListener = null;
    }
    //endregion

    //region Protected methods

    /**
     * Здесь указываем layout диалогового окна
     * @return Layout Id
     */
    protected abstract int getDialogView();

    /**
     * Кнопка ОК
     * @return Кнопка Ok
     */
    protected View getDialogOkButton()
    {
        return findViewById(R.id.ok_btn);
    }

    /**
     * Кнопка отмены
     * @return кнопка Отмены
     */
    protected View getDialogCancelButton()
    {
        return findViewById(R.id.cancel_btn);
    }

    /**
     * Действия производимые по нажатии на кнопку ОК
     * @return результат работы окна
     */
    protected abstract T doOnOkClick();

    /**
     * Действия производимые по нажатии на кнопку Отмена
     */
    protected abstract void doOnCancelClick();

    /**
     * Действия производимые перед открытием диалогового окна
     */
    protected abstract void doOnPreShow();
    //endregion
}
