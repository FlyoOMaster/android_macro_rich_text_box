package healthcare.powerfullspanstesting.dialogs.base;

/**
 * Created by trubnikov_ya on 07.04.2017.
 */

/**
 * Базовый интерфейс диалогового окна
 * @param <T> Возвращаемый тип в результате работы диалогового окна
 */
public interface IMacroDialog<T> {
    void setOnClickListener(DialogClickEvent<T> value);

    interface DialogClickEvent<T>{
        void onOk(T value);
        void onCancel();
    }
}
