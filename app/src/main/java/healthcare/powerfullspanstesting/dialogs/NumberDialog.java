package healthcare.powerfullspanstesting.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import healthcare.powerfullspanstesting.R;
import healthcare.powerfullspanstesting.dialogs.base.BaseMacroDialog;

/**
 * Created by trubnikov_ya on 07.04.2017.
 */

public class NumberDialog extends BaseMacroDialog<Integer> {
    //region Public constructors
    public NumberDialog(@NonNull Context context) {
        super(context);
    }
    //endregion

    //region Protected methods
    @Override
    protected int getDialogView() {
        return R.layout.macro_number_dialog;
    }

    @Override
    protected View getDialogOkButton() {
        return findViewById(R.id.ok_btn);
    }

    @Override
    protected View getDialogCancelButton() {
        return findViewById(R.id.cancel_btn);
    }

    @Override
    protected Integer doOnOkClick() {
        String stringNumber = ((EditText) findViewById(R.id.number_et)).getText().toString();
        return !TextUtils.isEmpty(stringNumber)
                ? Integer.parseInt(stringNumber)
                : null;
    }

    @Override
    protected void doOnCancelClick() {

    }

    @Override
    protected void doOnPreShow() {

    }
    //endregion
}
