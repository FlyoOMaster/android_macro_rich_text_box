package healthcare.powerfullspanstesting.dialogs;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.List;

import healthcare.powerfullspanstesting.R;
import healthcare.powerfullspanstesting.dialogs.base.BaseMacroDialog;
import healthcare.powerfullspanstesting.dialogs.collection.ListDialogItem;

/**
 * Created by trubnikov_ya on 07.04.2017.
 */

public class ListDialog<TResult extends ListDialogItem> extends BaseMacroDialog<ArrayList<TResult>> {
    //region Private fields
    private LinearLayout mCheckboxItemsContainerLl;
    private ArrayList<TResult> mItems;
    private boolean mIsCheckboxType;
    private ArrayList<Boolean> mOldItemsState;
    //endregion

    //region Public constructors
    public ListDialog(@NonNull Context context, ArrayList<TResult> items, boolean isCheckboxType) {
        super(context);
        init(items, isCheckboxType);
    }
    //endregion

    //region Protected fields
    @Override
    protected int getDialogView() {
        return R.layout.macro_list_dialog;
    }

    @Override
    protected ArrayList<TResult> doOnOkClick() {
        ArrayList<TResult> selectedItems = new ArrayList<>();
        for (TResult item : mItems)
        {
            if (item.isSelected)
            {
                selectedItems.add(item);
            }
        }


        return selectedItems;
    }

    @Override
    protected void doOnCancelClick() {
        int i = 0;

        for (ListDialogItem item : mItems)
        {
            item.isSelected = mOldItemsState.get(i++);
        }

        init(mItems, true);
    }

    @Override
    protected void doOnPreShow() {
        mOldItemsState = new ArrayList<>();

        for (ListDialogItem item : mItems) {
            mOldItemsState.add(item.isSelected);
        }
    }
    //endregion

    //region Private methods
    private void init(ArrayList<TResult> items, boolean isCheckboxType) {
        mCheckboxItemsContainerLl = (LinearLayout) findViewById(R.id.checkbox_items_container_ll);
        mItems = items;
        setType(isCheckboxType);

        if (mIsCheckboxType) {
            for (final TResult item : mItems) {
                final CheckBox checkBox = new CheckBox(getContext());
                checkBox.setChecked(item.isSelected);
                checkBox.setText(item.label);
                checkBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        item.isSelected = checkBox.isChecked();
                    }
                });
                mCheckboxItemsContainerLl.addView(checkBox);
            }
        }
    }

    private void setType(boolean isCheckboxType) {
        mIsCheckboxType = isCheckboxType;

        if (mIsCheckboxType) {
            mCheckboxItemsContainerLl.setVisibility(View.VISIBLE);
        }
    }
    //endregion
}
